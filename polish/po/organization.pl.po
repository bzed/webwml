# translation of organization.pl.po to polski
# Wojciech Zareba <wojtekz@comp.waw.pl>, 2007, 2008.
# Mirosław Gabruś <mirekgab@wp.pl>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: organization.pl\n"
"PO-Revision-Date: 2013-08-15 21:00+0200\n"
"Last-Translator: Mirosław Gabruś <mirekgab@wp.pl>\n"
"Language-Team: pl <>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Gtranslator 2.91.6\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "pełnomocnictwo"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "mianowanie"

#. One male delegate
#: ../../english/intro/organization.data:18
msgid "<void id=\"male\"/>delegate"
msgstr "<void id=\"male\"/>delegat"

#. One female delegate
#: ../../english/intro/organization.data:20
msgid "<void id=\"female\"/>delegate"
msgstr "<void id=\"female\"/>delegat"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:25
msgid "current"
msgstr "bieżący(a)"

#: ../../english/intro/organization.data:27
#: ../../english/intro/organization.data:29
msgid "member"
msgstr "członek"

#: ../../english/intro/organization.data:32
msgid "manager"
msgstr "zarządzający"

#: ../../english/intro/organization.data:34
msgid "SRM"
msgstr "ZWS"

#: ../../english/intro/organization.data:34
msgid "Stable Release Manager"
msgstr "Zarządzający Wydaniem Stabilnym"

#: ../../english/intro/organization.data:36
msgid "wizard"
msgstr "czarodziej"

#: ../../english/intro/organization.data:39
msgid "chairman"
msgstr "przewodniczący"

#: ../../english/intro/organization.data:42
msgid "assistant"
msgstr "asystent"

#: ../../english/intro/organization.data:44
msgid "secretary"
msgstr "sekretarz"

#: ../../english/intro/organization.data:53
#: ../../english/intro/organization.data:63
msgid "Officers"
msgstr "Oficerowie"

#: ../../english/intro/organization.data:54
#: ../../english/intro/organization.data:87
msgid "Distribution"
msgstr "Dystrybucja"

#: ../../english/intro/organization.data:55
#: ../../english/intro/organization.data:229
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:56
#: ../../english/intro/organization.data:232
#, fuzzy
#| msgid "Publicity"
msgid "Publicity team"
msgstr "Rozgłos"

#: ../../english/intro/organization.data:57
#: ../../english/intro/organization.data:300
msgid "Support and Infrastructure"
msgstr "Wsparcie i Infrastruktura"

#. formerly Custom Debian Distributions (CCDs); see https://blends.debian.org/blends/ch-about.en.html#s-Blends
#: ../../english/intro/organization.data:59
msgid "Debian Pure Blends"
msgstr "Czyste Mieszanki Debiana (Pure Blends)"

#: ../../english/intro/organization.data:66
msgid "Leader"
msgstr "Prowadzący"

#: ../../english/intro/organization.data:68
msgid "Technical Committee"
msgstr "Komitet Techniczny"

#: ../../english/intro/organization.data:82
msgid "Secretary"
msgstr "Sekretarz"

#: ../../english/intro/organization.data:90
msgid "Development Projects"
msgstr "Projekty Rozwoju"

#: ../../english/intro/organization.data:91
msgid "FTP Archives"
msgstr "Archiwa FTP"

#: ../../english/intro/organization.data:93
msgid "FTP Masters"
msgstr "Główny serwer FTP"

#: ../../english/intro/organization.data:99
msgid "FTP Assistants"
msgstr "Asystenci FTP"

#: ../../english/intro/organization.data:104
msgid "FTP Wizards"
msgstr "Kreator FTP"

#: ../../english/intro/organization.data:108
msgid "Backports"
msgstr "Backporty"

#: ../../english/intro/organization.data:110
msgid "Backports Team"
msgstr "Zespół Backportów"

#: ../../english/intro/organization.data:114
msgid "Individual Packages"
msgstr "Pojedyncze Pakiety"

#: ../../english/intro/organization.data:115
msgid "Release Management"
msgstr "Zarządzanie Wydaniem"

#: ../../english/intro/organization.data:117
msgid "Release Team"
msgstr "Zespół Wydania"

#: ../../english/intro/organization.data:130
msgid "Quality Assurance"
msgstr "Kontrola Jakości"

#: ../../english/intro/organization.data:131
msgid "Installation System Team"
msgstr "Zespół Systemu Instalacji"

#: ../../english/intro/organization.data:132
msgid "Release Notes"
msgstr "Informacje o Wydaniu"

#: ../../english/intro/organization.data:134
msgid "CD Images"
msgstr "Obrazy płyt"

#: ../../english/intro/organization.data:136
msgid "Production"
msgstr "Produkcja"

#: ../../english/intro/organization.data:144
msgid "Testing"
msgstr "Testowanie"

#: ../../english/intro/organization.data:146
msgid "Autobuilding infrastructure"
msgstr "Infrastruktura automatycznego budowania"

#: ../../english/intro/organization.data:148
msgid "Wanna-build team"
msgstr "Zespół wanna-build"

#: ../../english/intro/organization.data:156
msgid "Buildd administration"
msgstr "Administracja demona Buildd"

#: ../../english/intro/organization.data:175
msgid "Documentation"
msgstr "Dokumentacja"

#: ../../english/intro/organization.data:180
msgid "Work-Needing and Prospective Packages list"
msgstr "Lista pakietów potencjalnych i wymagających dopracowania"

#: ../../english/intro/organization.data:183
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:184
msgid "Ports"
msgstr "Adaptacje"

#: ../../english/intro/organization.data:219
msgid "Special Configurations"
msgstr "Konfiguracje Specjalne"

#: ../../english/intro/organization.data:222
msgid "Laptops"
msgstr "Laptopy"

#: ../../english/intro/organization.data:223
msgid "Firewalls"
msgstr "Firewalle"

#: ../../english/intro/organization.data:224
msgid "Embedded systems"
msgstr "Systemy wbudowane"

#: ../../english/intro/organization.data:237
msgid "Press Contact"
msgstr "Kontakt z prasą"

#: ../../english/intro/organization.data:239
msgid "Web Pages"
msgstr "Strony internetowe"

#: ../../english/intro/organization.data:249
msgid "Planet Debian"
msgstr "Planet Debian"

#: ../../english/intro/organization.data:254
msgid "Outreach"
msgstr ""

#: ../../english/intro/organization.data:258
msgid "Debian Women Project"
msgstr "Projekt Kobiecy Debiana"

#: ../../english/intro/organization.data:266
msgid "Anti-harassment"
msgstr "Przeci molestowaniu"

#: ../../english/intro/organization.data:272
msgid "Events"
msgstr "Wydarzenia"

#: ../../english/intro/organization.data:278
#, fuzzy
#| msgid "Technical Committee"
msgid "DebConf Committee"
msgstr "Komitet Techniczny"

#: ../../english/intro/organization.data:285
msgid "Partner Program"
msgstr "Program partnerski"

#: ../../english/intro/organization.data:290
msgid "Hardware Donations Coordination"
msgstr "Koordynacja darowizn sprzętu"

#: ../../english/intro/organization.data:303
msgid "User support"
msgstr "Wsparcie użytkowników"

#: ../../english/intro/organization.data:370
msgid "Bug Tracking System"
msgstr "System śledzenia błędów"

#: ../../english/intro/organization.data:375
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Administracja i archiwa list dyskusyjnych"

#: ../../english/intro/organization.data:383
msgid "New Members Front Desk"
msgstr "Dział Nowych Członków"

#: ../../english/intro/organization.data:389
msgid "Debian Account Managers"
msgstr "Menedżerowie kont Debiana"

#: ../../english/intro/organization.data:393
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""

#: ../../english/intro/organization.data:394
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Obsługa kluczy (PGP i GPG)"

#: ../../english/intro/organization.data:397
msgid "Security Team"
msgstr "Zespół ds. bezpieczeństwa"

#: ../../english/intro/organization.data:409
msgid "Consultants Page"
msgstr "Strony konsultatnów"

#: ../../english/intro/organization.data:414
msgid "CD Vendors Page"
msgstr "Strona sprzedawców płyt CD"

#: ../../english/intro/organization.data:417
msgid "Policy"
msgstr "Polityka"

#: ../../english/intro/organization.data:422
msgid "System Administration"
msgstr "Administracja systemu"

#: ../../english/intro/organization.data:423
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"To jest adres, którego można użyć, gdy masz problem z maszynami należącymi "
"do projektu Debian, w tym problemy z hasłami lub prośby o zainstalowanie "
"pakietu."

#: ../../english/intro/organization.data:432
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Jeśli masz problem sprzętowy z maszynami Debiana, zobacz stronę <a href="
"\"https://db.debian.org/machines.cgi\">Maszyny Debiana</a>, powinna ona "
"zawierać informacje o administratorach poszczególnych maszyn."

#: ../../english/intro/organization.data:433
msgid "LDAP Developer Directory Administrator"
msgstr "Administracja Katalogiem Deweloperów LDAP"

#: ../../english/intro/organization.data:434
msgid "Mirrors"
msgstr "Serwery lustrzane"

#: ../../english/intro/organization.data:441
msgid "DNS Maintainer"
msgstr "Obsługa DNS"

#: ../../english/intro/organization.data:442
msgid "Package Tracking System"
msgstr "System śledzenia pakietów"

#: ../../english/intro/organization.data:444
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:450
#, fuzzy
#| msgid "<a href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr "Wniosek o użycie <a href=\"m4_HOME/trademark\">znaku handlowego</a>"

#: ../../english/intro/organization.data:453
#, fuzzy
#| msgid "Alioth administrators"
msgid "Salsa administrators"
msgstr "Administracja systemu Alioth"

#: ../../english/intro/organization.data:457
msgid "Alioth administrators"
msgstr "Administracja systemu Alioth"

#: ../../english/intro/organization.data:470
msgid "Debian for children from 1 to 99"
msgstr "Debian dla dzieci od lat 1 do 99"

#: ../../english/intro/organization.data:473
msgid "Debian for medical practice and research"
msgstr "Debian dla medycyny i badań"

#: ../../english/intro/organization.data:476
msgid "Debian for education"
msgstr "Debian dla edukacji"

#: ../../english/intro/organization.data:481
msgid "Debian in legal offices"
msgstr "Debian w kancelariach prawnych"

#: ../../english/intro/organization.data:485
msgid "Debian for people with disabilities"
msgstr "Debian dla niepełnosprawnych"

#: ../../english/intro/organization.data:489
msgid "Debian for science and related research"
msgstr "Debian dla nauk ścisłych i powiązanych badań"

#: ../../english/intro/organization.data:492
#, fuzzy
#| msgid "Debian for education"
msgid "Debian for astronomy"
msgstr "Debian dla edukacji"

#~ msgid "Testing Security Team"
#~ msgstr "Zespół ds. bezpieczeństwa dla edycji testowej"

#~ msgid "Security Audit Project"
#~ msgstr "Projekt audytu bezpieczeństwa"

#~ msgid "current Debian Project Leader"
#~ msgstr "obecny Lider Projektu Debiana"

#~ msgid "Summer of Code 2013 Administrators"
#~ msgstr "Administratorzy Summer of Code 2013"

#~ msgid "DebConf chairs"
#~ msgstr "przewodniczący DebConfa"

#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Obsługa kluczy Deweloperów Debiana (DM - Debian Maintainer)"

#~ msgid "Bits from Debian"
#~ msgstr "Informacje Debiana"

#~ msgid "Publicity"
#~ msgstr "Rozgłos"

#~ msgid "Auditor"
#~ msgstr "Audyt"

#~ msgid "Live System Team"
#~ msgstr "Zespół Systemu Live"
